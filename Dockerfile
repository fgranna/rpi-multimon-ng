FROM sysrun/rpi-rtl-sdr-base:0.4

MAINTAINER Frederik Granna

WORKDIR /tmp

RUN apt-get update && \
    apt-get install -y libpulse-dev libx11-dev qt4-qmake --no-install-recommends && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

ENV commit_id 04847c5a65581228a55c0131f2d2ad02e3138697

RUN git clone https://github.com/EliasOenal/multimon-ng.git && \
    cd /tmp/multimon-ng && \
    git reset --hard $commit_id && \
    mkdir /tmp/multimon-ng/build && \
    cd /tmp/multimon-ng/build && \
    qmake ../multimon-ng.pro && \
    make && \
    make install && \
    rm -rf /tmp/multimon-ng

ENTRYPOINT ["multimon-ng"]
